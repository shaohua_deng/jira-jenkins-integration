/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.com.marvelution.jira.plugins.jenkins.rest;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.codec.binary.Base64;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Base REST Resource test case
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class BaseRESTTest {

	protected static final String AUTH_HEADER = "Authorization";

	protected final ApplicationProperties properties;
	protected final Client client;

	protected BaseRESTTest(ApplicationProperties properties) {
		this.properties = properties;
		client = Client.create();
	}

	/**
	 * Getter for a service with an admin user authentication header
	 *
	 * @param path the path elements
	 * @return the {@link WebResource.Builder}
	 */
	protected WebResource.Builder getAdminService(String... path) {
		return getService(path).header(AUTH_HEADER, getAuthHeaderFormUser("admin"));
	}

	/**
	 * Getter for a service with an user authentication header
	 *
	 * @param path the path elements
	 * @return the {@link WebResource.Builder}
	 */
	protected WebResource.Builder getUserService(String... path) {
		return getService(path).header(AUTH_HEADER, getAuthHeaderFormUser("user"));
	}

	/**
	 * Getter for a service
	 *
	 * @param path the path elements
	 * @return the {@link WebResource.Builder}
	 */
	protected WebResource getService(String... path) {
		assertThat(path, notNullValue());
		assertThat(path.length > 0, is(true));
		WebResource resource = client.resource(properties.getString(APKeys.JIRA_BASEURL)).path("rest").path("jenkins")
				.path("1.0");
		for (String p : path) {
			resource = resource.path(p);
		}
		return resource;
	}

	/**
	 * Helper method to create the Basic Authentication header for the user given
	 *
	 * @param user the username (and password) of the user
	 * @return the Basic authentication header string
	 */
	protected String getAuthHeaderFormUser(String user) {
		String auth = user + ":" + user;
		return "Basic " + Base64.encodeBase64URLSafeString(auth.getBytes());
	}

}
