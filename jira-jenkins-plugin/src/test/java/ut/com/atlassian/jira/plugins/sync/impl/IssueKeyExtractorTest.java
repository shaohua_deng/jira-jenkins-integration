/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package ut.com.atlassian.jira.plugins.sync.impl;

import com.atlassian.jira.plugins.dvcs.sync.impl.IssueKeyExtractor;
import org.junit.Test;

import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link IssueKeyExtractor}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class IssueKeyExtractorTest {

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a {@code null} message
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithNullMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys(null);
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a {@code empty} message
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithEmptyMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a single issue key on a single comment line
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithSingleLineSingleKeyMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("JJI-4");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(1));
		assertThat(keys.contains("JJI-4"), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a single issue key on a single comment line
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithSingleLineSingleKeyWithCommentMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("Implemented a test case for JJI-4");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(1));
		assertThat(keys.contains("JJI-4"), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with multiple issue keys on a single comment line
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithSingleLineMultipleKeysMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("JJI-4, JJI-1 JJI-3");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(3));
		assertThat(keys.contains("JJI-4"), is(true));
		assertThat(keys.contains("JJI-1"), is(true));
		assertThat(keys.contains("JJI-3"), is(true));
		assertThat(keys.contains("JJI-2"), is(false));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with multiple issue keys on multiple comment lines
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithMultipleLinesMultipleKeysMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("JJI-4\nJJI-1\nJJI-3");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(3));
		assertThat(keys.contains("JJI-4"), is(true));
		assertThat(keys.contains("JJI-1"), is(true));
		assertThat(keys.contains("JJI-3"), is(true));
		assertThat(keys.contains("JJI-2"), is(false));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with multiple issue keys on multiple comment lines
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithMultipleLinesMultipleKeysMessage2() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("Empty first line\nJJI-1\nJJI-3");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(2));
		assertThat(keys.contains("JJI-4"), is(false));
		assertThat(keys.contains("JJI-1"), is(true));
		assertThat(keys.contains("JJI-3"), is(true));
		assertThat(keys.contains("JJI-2"), is(false));
	}

}
