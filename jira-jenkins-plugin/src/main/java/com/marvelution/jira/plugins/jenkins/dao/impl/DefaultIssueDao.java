/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import com.marvelution.jira.plugins.jenkins.dao.IssueDao;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.marvelution.jira.plugins.jenkins.ao.IssueMapping.*;

/**
 * Default {@link IssueDao} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultIssueDao implements IssueDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSiteDao.class);

	private final ActiveObjects ao;
	private final IssueManager issueManager;

	public DefaultIssueDao(ActiveObjects ao, IssueManager issueManager) {
		this.ao = ao;
		this.issueManager = issueManager;
	}

	@Override
	public Iterable<String> getIssueKeysByBuild(final Build build) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(BUILD_ID + " = ?",
						build.getId()));
				return Arrays.asList(mappings);
			}
		});
		return transformNoDuplicates(mappings, new Function<IssueMapping, String>() {
			@Override
			public String apply(@Nullable IssueMapping input) {
				return input != null ? input.getIssueKey() : null;
			}
		});
	}

	@Override
	public Iterable<String> getIssueKeysByJob(final Job job) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(JOB_ID + " = ?",
						job.getId()));
				return Arrays.asList(mappings);
			}
		});
		return transformNoDuplicates(mappings, new Function<IssueMapping, String>() {
			@Override
			public String apply(@Nullable IssueMapping input) {
				return input != null ? input.getIssueKey() : null;
			}
		});
	}

	@Override
	public Iterable<String> getProjectKeysByBuild(final Build build) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(BUILD_ID + " = ?",
						build.getId()));
				return Arrays.asList(mappings);
			}
		});
		return transformNoDuplicates(mappings, new Function<IssueMapping, String>() {
			@Override
			public String apply(@Nullable IssueMapping input) {
				return input != null ? input.getProjectKey() : null;
			}
		});
	}

	@Override
	public int getIssueLinkCount(final Build build) {
		return ao.executeInTransaction(new TransactionCallback<Integer>() {
			@Override
			public Integer doInTransaction() {
				return ao.count(IssueMapping.class, Query.select().where(BUILD_ID + " = ?", build.getId()));
			}
		});
	}

	/**
	 * Transform the given {@link List} of {@link IssueMapping}s using the given {@link Function}
	 *
	 * @param mappings the {@link List} of {@link IssueMapping}s to transform
	 * @param function the transformation {@link Function} to use
	 * @param <I>      the resulting item Type
	 * @return the transformed collection
	 */
	private <I> Iterable<I> transformNoDuplicates(List<IssueMapping> mappings, Function<IssueMapping, I> function) {
		return Sets.newHashSet(Iterables.transform(mappings, function));
	}

	@Override
	public boolean link(final Build build, String issueKey) {
		checkNotNull(build, "build may not be null");
		checkNotNull(issueKey, "issueKey may not be null");
		final Issue issue = issueManager.getIssueObject(issueKey);
		if (issue != null) {
			LOGGER.debug("Linking {} to {}", issue.getKey(), build.toString());
			ao.executeInTransaction(new TransactionCallback<Object>() {
				@Override
				public Object doInTransaction() {
					Map<String, Object> map = Maps.newHashMap();
					map.put(JOB_ID, build.getJobId());
					map.put(BUILD_ID, build.getId());
					map.put(ISSUE_KEY, issue.getKey());
					map.put(BUILD_DATE, build.getTimestamp());
					map.put(PORJECT_KEY, issue.getProjectObject().getKey());
					ao.create(IssueMapping.class, map);
					return null;
				}
			});
			return true;
		} else {
			LOGGER.debug("No issue with key {} on this JIRA instance, skipping link", issueKey);
			return false;
		}
	}

	@Override
	public void unlinkForIssueKey(final String issueKey) {
		checkNotNull(issueKey, "issueKey may not be null");
		final IssueMapping[] mappings = getIssueMappingsByIssueKey(issueKey);
		if (mappings != null && mappings.length > 0) {
			LOGGER.debug("Unlinked issue {} from all known builds", issueKey);
			ao.executeInTransaction(new TransactionCallback<Object>() {
				@Override
				public Object doInTransaction() {
					ao.delete(mappings);
					return null;
				}
			});
		}
	}

	@Override
	public void unlinkForProjectKey(final String projectKey) {
		checkNotNull(projectKey, "projectKey may not be null");
		final IssueMapping[] mappings = getIssueMappingsByProjectKey(projectKey);
		if (mappings != null && mappings.length > 0) {
			LOGGER.debug("Unlinked project {} from all known builds", projectKey);
			ao.executeInTransaction(new TransactionCallback<Object>() {
				@Override
				public Object doInTransaction() {
					ao.delete(mappings);
					return null;
				}
			});
		}
	}

	private IssueMapping[] getIssueMappingsByIssueKey(final String issueKey) {
		return ao.executeInTransaction(new TransactionCallback<IssueMapping[]>() {
			@Override
			public IssueMapping[] doInTransaction() {
				return ao.find(IssueMapping.class, Query.select().where(ISSUE_KEY + " = ?", issueKey));
			}
		});
	}

	private IssueMapping[] getIssueMappingsByProjectKey(final String projectKey) {
		return ao.executeInTransaction(new TransactionCallback<IssueMapping[]>() {
			@Override
			public IssueMapping[] doInTransaction() {
				return ao.find(IssueMapping.class, Query.select().where(PORJECT_KEY + " = ?", projectKey));
			}
		});
	}

}
