/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jira.plugins.jenkins.applinks;

import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;

/**
 * AppLinksManifestProducer for Jenkins applications
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsManifestProducer extends DefaultManifestProducer<JenkinsApplicationType> {

	public JenkinsManifestProducer(RequestFactory<Request<Request<?, Response>, Response>> requestFactory,
	                               ManifestRetriever manifestRetriever, TypeAccessor typeAccessor) {
		super(requestFactory, manifestRetriever, typeAccessor);
	}

	@Override
	protected Class<JenkinsApplicationType> getApplicationType() {
		return JenkinsApplicationType.class;
	}

}
