/*
 * Jenkins Plugin for JIRA
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function autoLinkSite(siteId, enabled) {
	AJS.$.ajax({
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		url: AJS.contextPath() + "/rest/jenkins/1.0/site/" + siteId + "/autolink",
		data: '{ "payload" : "' + enabled + '"}'
	}).error(function () {
			AJS.$('#site-auto-link-' + siteId).removeClass('checked').attr('aria-checked', 'false');
		});
}

function autoLinkJob(jobId, checkboxId) {
	var checkedValue = AJS.$("#" + checkboxId).is(":checked");
	AJS.$("#" + checkboxId).attr("disabled", "disabled");
	AJS.$.ajax({
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		url: AJS.contextPath() + "/rest/jenkins/1.0/job/" + jobId + "/autolink",
		data: '{ "payload" : "' + checkedValue + '"}',
		success: function () {
			AJS.$("#" + checkboxId).removeAttr("disabled");
		}
	}).error(function () {
			AJS.$("#" + checkboxId).removeAttr("disabled");
			AJS.$("#" + checkboxId).removeAttr("checked");
		});
}

function syncJob(jobId) {
	if (AJS.$("#job_sync_" + jobId + " .job-sync-icon").hasClass("active")) {
		return;
	}
	AJS.$.ajax({
		type: 'POST',
		dataType: 'json',
		url: AJS.contextPath() + "/rest/jenkins/1.0/job/" + jobId + "/sync",
		success: function () {
			AJS.$("#job_syncicon_" + jobId).addClass("active");
		}
	}).error(function (err) {
			showSyncMessage(jobId, err.responseText, false);
		});
}

function updateSyncStatus(job) {
	AJS.$.ajax({
		type: 'GET',
		dataType: 'json',
		url: AJS.contextPath() + "/rest/jenkins/1.0/job/" + job.id + "/sync/status",
		success: function (data) {
			if (data == null || data.finished) {
				// We are done
				AJS.$("#job_syncicon_" + job.id).removeClass("active");
				showSyncMessage(job.id, job.lastBuild, true);
			} else {
				if (!AJS.$("#job_syncicon_" + job.id).hasClass("active")) {
					AJS.$("#job_syncicon_" + job.id).addClass("active");
				}
				var message = AJS.I18n.getText("synchronizing") + " <strong>" + data.buildCount + "</strong> "
					+ AJS.I18n.getText("builds") + ", <stong>" + data.issueCount + "</stong> "
					+ AJS.I18n.getText("issues");
				if (data.syncErrorCount > 0) {
					message += ", <span class='error'>" + AJS.I18n.getText("errors") + " <strong>" + data.buildCount + "</strong></span>";
				}
				showSyncMessage(job.id, message, true);
			}
		}
	}).error(function (xhr, message) {
			AJS.$("#job_syncicon_" + job.id).removeClass("active");
			showSyncMessage(job.id, "Failed to update the synchronisation status: " + message, false);
		    AJS.log("Sync Update Failure for Job: " + job.name, xhr);
		});
}

function showSyncMessage(jobId, message, successful) {
	if (successful) {
		AJS.$("#job_sync_" + jobId + " .job-sync-status").show().html(message);
		AJS.$("#job_sync_" + jobId + " .job-sync-error").hide();
	} else {
		AJS.$("#job_sync_" + jobId + " .job-sync-status").hide();
		AJS.$("#job_sync_" + jobId + " .job-sync-error").show().html(message);
	}
}

function retrieveSyncStatus() {
	AJS.$.ajax({
		type: 'GET',
		dataType: 'json',
		url: AJS.contextPath() + "/rest/jenkins/1.0/job",
		data: {
			includeDeleted: true
		},
		success: function (data) {
			if (data != null && data.job != null) {
				AJS.$.each(data.job, function (a, job) {
					if (job.linked) {
						updateSyncStatus(job);
					}
				});
			}
			window.setTimeout(retrieveSyncStatus, 4000);
		}
	});
}

function initializeSiteDropdown(siteId) {
	AJS.$('#site-auto-link-' + siteId).on({
		'aui-dropdown2-item-check': function () {
			autoLinkSite(siteId, true);
		},
		'aui-dropdown2-item-uncheck': function () {
			autoLinkSite(siteId, false);
		}
	});
}

function checkSiteStatus(siteId) {
	AJS.$.ajax({
		type: 'GET',
		dataType: 'json',
		url: AJS.contextPath() + "/rest/jenkins/1.0/site/" + siteId + "/status",
		success: function (data) {
			var status = data.status || "";
			var site = data.site || {};
			var context = '#message-bar-jenkins-' + siteId;
			if (status == 'OFFLINE') {
				AJS.messages.warning(context, {
					body: site.name + ' seems to be offline'
				});
			} else if (status == 'NOT_ACCESSIBLE') {
				AJS.messages.error(context, {
					title: 'Authentication Error',
					body: 'An authentication error occurred connecting with <b>' + site.name + '</b>'
						+ '. Click <a href="' + AJS.contextPath() + '/plugins/servlet/applinks/listApplicationLinks">here</a> '
						+ 'to check the application link.'
				});
			} else if (status == 'ONLINE_NO_PLUGIN') {
				AJS.messages.hint(context, {
					title: 'The plugin JIRA Plugin for Jenkins doesn\'t seem to be installed on ' + site.name,
					body: 'Consider installing it to optimize the integration with JIRA and allow the use of '
						+ '<a target="_blank" href="https://marvelution.atlassian.net/wiki/x/KQCp">advanced features'
					    + '</a>.<br/><br/>Download it <a href="' + data.message + '">HERE</a> and install it using the '
						+ '<a href="https://wiki.jenkins-ci.org/display/JENKINS/Plugins#Plugins-Byhand">upload method</a>'
				});
			}
		}
	});
}

function deleteJobBuilds(jobId) {
	AJS.$.ajax({
		type: 'DELETE',
		dataType: 'json',
		url: AJS.contextPath() + "/rest/jenkins/1.0/job/" + jobId + "/builds",
		success: function () {
			showSyncMessage(jobId, '0', true);
		}
	}).error(function (err) {
			showSyncMessage(jobId, err.responseText, false);
		});
}


function restartJobSync(jobId) {
	AJS.$.ajax({
		type: 'POST',
		dataType: 'json',
		url: AJS.contextPath() + "/rest/jenkins/1.0/job/" + jobId + "/rebuild",
		success: function () {
			showSyncMessage(jobId, '0', true);
		}
	}).error(function (err) {
			showSyncMessage(jobId, err.responseText, false);
		});
}
