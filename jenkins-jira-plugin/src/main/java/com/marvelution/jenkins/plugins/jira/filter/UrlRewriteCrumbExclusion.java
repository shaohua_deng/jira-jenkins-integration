/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.filter;

import com.marvelution.jenkins.plugins.jira.JIRAPlugin;
import hudson.Extension;
import hudson.security.csrf.CrumbExclusion;
import org.tuckey.web.filters.urlrewrite.Rule;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Logger;

/**
 * {@link hudson.security.csrf.CrumbExclusion} implementation to disable the Crumb protection for all UrlRewrite related URLs
 *
 * @author Mark Rekveld
 * @since 1.4.4
 */
@Extension
public class UrlRewriteCrumbExclusion extends CrumbExclusion {

	private static final Logger LOGGER = Logger.getLogger(UrlRewriteCrumbExclusion.class.getName());

	@SuppressWarnings("unchecked")
	@Override
	public boolean process(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		UrlRewriteFilter filter = JIRAPlugin.getFilter(UrlRewriteFilter.class);
		if (filter != null) {
			String url = filter.getRewriter().getPathWithinApplication(request);
			for (Rule rule : (List<Rule>) filter.getRewriter().getConf().getRules()) {
				try {
					if (rule.matches(url, request, response) != null) {
						LOGGER.info("Disabling crumb protection for JIRA request: " + url);
						chain.doFilter(request, response);
						return true;
					}
				} catch (InvocationTargetException ignore) {}
			}
		}
		return false;
	}

}
