/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.action;

import hudson.Extension;
import hudson.model.UnprotectedRootAction;

/**
 * OAuth root action
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
@Extension
public class OAuthRootAction implements UnprotectedRootAction {

	public static final String URL_NAME = "oauth";

	@Override
	public String getIconFileName() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	/**
	 * Lookup the OAuth action to forward the request to
	 *
	 * @param token the action token
	 * @return the {@link com.marvelution.jenkins.plugins.jira.oauth.action.OAuthAction} to forward to, may be {@code null}
	 */
	public Object getDynamic(String token) {
		for (OAuthAction action : OAuthAction.all()) {
			if (action.getUrlName().equals(token) || action.getUrlName().equals("/" + token)) {
				return action;
			}
		}
		return null;
	}

}
