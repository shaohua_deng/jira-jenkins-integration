/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.model.Manifest;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import hudson.Extension;
import hudson.model.Hudson;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URI;

/**
 * Handler for the {@code /rest/applinks/1.0/manifest} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class ManifestAction extends ApplicationLinksAction {

	public static final String ACTION = "manifest";

	@Override
	public String getUrlName() {
		return ACTION;
	}

	public void doIndex(StaplerRequest request, StaplerResponse response) throws Exception {
		XStreamUtils.writeXmlToResponse(getOwnManifest(request), response);
	}

	/**
	 * Get the {@link com.marvelution.jenkins.plugins.jira.model.Manifest} of the local instance
	 *
	 * @param request the {@link javax.servlet.http.HttpServletRequest}, used to determine the application root url
	 * @return the Manifest
	 * @throws IOException in case of application store loading issues
	 */
	public static Manifest getOwnManifest(HttpServletRequest request) throws IOException {
		return Manifest.create(URI.create(ApplicationLinkStore.getStore().getApplicationUrl()), Hudson.getVersion().toString(),
				ApplicationLinkStore.getStore().getApplicationName(), Hudson.getInstance().getSecurityRealm().allowsSignup());
	}

	/**
	 * Get the {@link com.marvelution.jenkins.plugins.jira.model.Manifest} of the remote system
	 *
	 * @param remoteUrl the base URL of the remote Application Link system
	 * @return the {@link com.marvelution.jenkins.plugins.jira.model.Manifest}
	 */
	public static Manifest getRemoteManifest(String remoteUrl) throws IOException {
		WebResource webResource = new Client().resource(remoteUrl).path("/rest/applinks/1.0/").path(ACTION);
		ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML_TYPE).get(ClientResponse.class);
		return XStreamUtils.getEntityFromRequest(clientResponse.getEntityInputStream(), Manifest.class);
	}

}
