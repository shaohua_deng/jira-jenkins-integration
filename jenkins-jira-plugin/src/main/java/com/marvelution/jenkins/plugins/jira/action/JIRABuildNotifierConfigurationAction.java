/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.action;

import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import com.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier;
import com.marvelution.jenkins.plugins.jira.store.ApplicationLinkStore;
import hudson.model.*;
import hudson.security.AccessDeniedException2;
import hudson.tasks.Publisher;
import hudson.util.DescribableList;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * {@link Action} implementation that is used by JIRA to configure a {@link JIRABuildNotifier} for its job
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JIRABuildNotifierConfigurationAction implements Action {

	public static final String APPLINK_SCHEME = "applink";
	static final String POST = "POST";
	static final String URL_NAME = "jira";
	static final String APP_ID_PARAMETER = "appId";
	static final String JOB_ID_PARAMETER = "jobId";
	static final String POST_URL_PARAMETER = "postUrl";
	private static final Logger LOGGER = Logger.getLogger(JIRABuildNotifierConfigurationAction.class.getName());
	private final AbstractProject target;

	public JIRABuildNotifierConfigurationAction(AbstractProject target) {
		this.target = target;
	}

	@Override
	public String getIconFileName() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	/**
	 * Dynamic method implementation that is called then this action is visited.
	 * It will add a new or replace the existing {@link JIRABuildNotifier}
	 *
	 * @param request  the {@link StaplerRequest}
	 * @param response the {@link StaplerResponse}
	 * @throws IOException      in case of IO errors
	 * @throws ServletException in case of Servlet Errors
	 */
	@SuppressWarnings("unchecked")
	public void doDynamic(final String token, final StaplerRequest request, final StaplerResponse response) throws
			IOException, ServletException {
		if (target.hasPermission(Item.CONFIGURE)) {
			String postUrl = getPostUrlFromRequest(request);
			if (StringUtils.isNotBlank(postUrl)) {
				DescribableList<Publisher, Descriptor<Publisher>> publishers = target.getPublishersList();
				JIRABuildNotifier newNotifier = new JIRABuildNotifier(postUrl);
				JIRABuildNotifier currentNotifier = publishers.get(JIRABuildNotifier.class);
				if (currentNotifier == null) {
					publishers.add(newNotifier);
					response.getWriter().write("Successfully added a new JIRA Build Notifier");
				} else if (!currentNotifier.postUrl.equals(postUrl)) {
					publishers.replace(currentNotifier, newNotifier);
					response.getWriter().write("Successfully replaced the existing JIRA Build Notifier");
				} else {
					response.getWriter().write("JIRA Build Notifier is already configured");
				}
				LOGGER.log(Level.FINE, "Configured postUrl [" + postUrl + "] for " + target.getName());
			} else {
				LOGGER.warning("Invalid call to configure a JIRABuildNotifier for " + target.getName());
			}
		} else {
			throw new AccessDeniedException2(Hudson.getAuthentication(), Item.CONFIGURE);
		}
	}

	/**
	 * Get the post URL that the {@link JIRABuildNotifier} should post the build result to. It will try to generate a post URL that uses
	 * the Application Links framework.
	 *
	 * @param request the {@link StaplerRequest} to get the post url from
	 * @return the {@link String postUrl}, may be {@code null}
	 */
	private String getPostUrlFromRequest(StaplerRequest request) {
		String postUrl = null;
		if (POST.equalsIgnoreCase(request.getMethod())) {
			if (request.hasParameter(APP_ID_PARAMETER) && request.hasParameter(JOB_ID_PARAMETER)) {
				try {
					String appId = request.getParameter(APP_ID_PARAMETER);
					ApplicationLink link = getApplicationLink(appId);
					if (link != null) {
						int jobId = Integer.parseInt(request.getParameter(JOB_ID_PARAMETER));
						postUrl = new URI(APPLINK_SCHEME, null, link.getId(), jobId, null, null, null).toString();
					} else {
						LOGGER.warning("No application link found under id: " + appId);
					}
				} catch (NumberFormatException e) {
					LOGGER.log(Level.SEVERE, "Invalid Job ID parameter given in request", e);
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, "Failed to load Application Link store", e);
				} catch (URISyntaxException e) {
					LOGGER.log(Level.SEVERE, "Failed to generate applink post Url for build notifications", e);
				}
			}
			if (postUrl == null && request.hasParameter(POST_URL_PARAMETER)) {
				postUrl = request.getParameter(POST_URL_PARAMETER);
			}
		}
		return postUrl;
	}

	ApplicationLink getApplicationLink(String appId) throws IOException {
		return ApplicationLinkStore.getStore().get(appId);
	}

}
