/*
 * JIRA Plugin for Jenkins
 * Copyright (C) 2012 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.jenkins.plugins.jira.oauth.xstream;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import hudson.model.User;

import java.security.Principal;
import java.util.Collections;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * {@link com.thoughtworks.xstream.converters.Converter} to convert {@link java.security.Principal} objects
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class PrincipalConverter implements Converter {

	private static final Logger LOGGER = Logger.getLogger(PrincipalConverter.class.getName());

	@Override
	public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
		writer.setValue(((Principal) value).getName());
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		final String name = reader.getValue();
		try {
			return new UserPrincipal(User.get(name, false, Collections.emptyMap()));
		} catch (Exception e) {
			LOGGER.warning("Failed to load principal for name: " + name);
			return null;
		}
	}

	@Override
	public boolean canConvert(Class type) {
		return Principal.class.isAssignableFrom(type);
	}

	public static class UserPrincipal implements Principal {

		private final User user;

		public UserPrincipal(User user) {
			this.user = checkNotNull(user, "user cannot be null");
		}

		public User getUser() {
			return user;
		}

		@Override
		public String getName() {
			return user.getId();
		}

	}

}
